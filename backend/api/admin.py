from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.db.models import aggregates, F, QuerySet
import django_countries
from collections import Counter, defaultdict
from .models import *


@admin.decorators.register(Accommodation)
class AccommodationAdmin(admin.ModelAdmin):
    list_display = ("id", "description", "days_reserved")

    def get_queryset(self, request):
        # calculating this value yet again because django admin does not support sorting on non-db fields
        qs: QuerySet = super().get_queryset(request)
        return qs.annotate(
            a__days_reserved=aggregates.Sum(F("reservations__date_end") - F("reservations__date_start"))
        ).order_by("-a__days_reserved")


@admin.decorators.register(AccommodationType)
class AccommodationTypeAdmin(admin.ModelAdmin):
    list_display = ("id", "view", "type", "capacity_min", "capacity_max")


@admin.decorators.register(Reservation)
class ReservationAdmin(admin.ModelAdmin):
    list_display = ("user", "accommodation", "date_start", "date_end", "parking", "wifi", "tv")


@admin.decorators.register(User)
class AccommodationAdmin(admin.ModelAdmin):
    list_display = ("email", "first_name", "last_name", "country")


class CountrySummary(User):
    class Meta:
        proxy = True
        verbose_name = "Pregled država"
        verbose_name_plural = verbose_name


@admin.decorators.register(CountrySummary)
class CountrySummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/country_summary.html'

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )

        try:
            qs = response.context_data["cl"].queryset
        except (AttributeError, KeyError):
            return response

        countries = dict(django_countries.countries)

        response.context_data["summary"] = map(
            lambda x: (countries[x[0]] if x[0] in countries else "", x[1]),
            Counter([user.country.code for user in User.objects.all()]).most_common()
        )

        return response


class CitySummary(User):
    class Meta:
        proxy = True
        verbose_name = "Pregled gradova"
        verbose_name_plural = verbose_name


@admin.decorators.register(CitySummary)
class CitySummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/city_summary.html'

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )

        try:
            qs = response.context_data["cl"].queryset
        except (AttributeError, KeyError):
            return response

        response.context_data["summary"] = Counter([user.city for user in User.objects.all()]).most_common()

        return response


class ExtrasSummary(Reservation):
    class Meta:
        proxy = True
        verbose_name = "Pregled dodatnih usluga"
        verbose_name_plural = verbose_name


@admin.decorators.register(ExtrasSummary)
class ExtrasSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/extras_summary.html'

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )

        try:
            qs = response.context_data["cl"].queryset
        except (AttributeError, KeyError):
            return response

        countries = defaultdict(lambda: "", django_countries.countries)

        response.context_data["summary"] = [
            (
                countries[country_code],
                len(Reservation.objects.filter(user__country=country_code)),
                len(Reservation.objects.filter(user__country=country_code, wifi=True)),
                len(Reservation.objects.filter(user__country=country_code, parking=True)),
                len(Reservation.objects.filter(user__country=country_code, tv=True)),
            )
            for country_code in set(user.country.code for user in User.objects.all())
        ]

        return response

admin.site.site_header = _("Hotel 'Kod nas je najljepše' - administracija")
admin.site.site_title = _("Admin")
admin.site.index_title = _("Administracijska stranica")

