from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register('accommodations', views.AccommodationViewSet)
router.register('types', views.AccommodationTypeViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('test', views.test_view),
    path('reserve', views.reserve),
    path('register', views.register),
    path('confirm-email/<slug:key>', views.confirm_email, name="confirm-email"),
    path('login', views.login),
    path('logout', views.logout),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
