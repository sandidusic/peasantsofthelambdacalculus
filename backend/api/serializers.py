from django.contrib.auth.models import User
from rest_framework import serializers
from django_countries import countries
from django.conf import settings

from . import models


class ReservationSerializerPublic(serializers.ModelSerializer):
    class Meta:
        model = models.Reservation
        fields = ("date_start", "date_end")


class AccommodationSerializer(serializers.ModelSerializer):
    reservations = ReservationSerializerPublic(many=True, read_only=True)

    class Meta:
        model = models.Accommodation
        fields = "__all__"


class AccommodationTypeSerializer(serializers.ModelSerializer):
    accommodations = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = models.AccommodationType
        fields = "__all__"


class ChildrenSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Children
        fields = ("number", "age_class")


class ReservationSerializerWrite(serializers.ModelSerializer):
    accommodation_type = serializers.PrimaryKeyRelatedField(queryset=models.AccommodationType.objects.all(), write_only=True)
    children = ChildrenSerializer(many=True)

    class Meta:
        model = models.Reservation
        fields = ("accommodation_type", "children", "adults", "date_start", "date_end", "parking", "wifi", "tv")

    def create(self, validated_data):
        validated_data.pop('accommodation_type')
        children_data = validated_data.pop('children')
        reservation = models.Reservation.objects.create(**validated_data)
        for children in children_data:
            models.Children.objects.create(reservation=reservation, **children)
        return reservation


class UserSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    email = serializers.EmailField()

    def create(self, validated_data):
        password = validated_data.pop("password")
        return models.User.objects.create_user(
            username=validated_data["email"],  # use emails as usernames since there are no usernames in the spec
            password=password,
            **validated_data
        )

    class Meta:
        model = models.User
        fields = ("password", "first_name", "last_name", "email", "address", "city", "country", "telephone")
