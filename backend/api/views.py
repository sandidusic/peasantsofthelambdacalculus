from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
import rest_framework.authtoken.views
from django.db import transaction
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.core.mail import send_mail
from django.conf import settings
from django.urls import reverse
import datetime
from datetime import date
import json
import random
import string

from . import models
from . import serializers
from . import errors
from .errors import Error
from . import optimize


class AccommodationViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Accommodation.objects.all()
    serializer_class = serializers.AccommodationSerializer


class AccommodationTypeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.AccommodationType.objects.all()
    serializer_class = serializers.AccommodationTypeSerializer


@csrf_exempt
def login(request):
    # DRE api request/repsonse and httprequest/response aren't compatible so hacks will ensue
    username = json.loads(request.body)["username"]
    print(username)
    users = models.User.objects.filter(username=username)
    if len(users) == 0 or not users[0].email_confirmed:
        return HttpResponse(json.dumps(errors.error_response(Error.EMAIL_NOT_CONFIRMED).data), status=400)

    return rest_framework.authtoken.views.obtain_auth_token(request)


@api_view(["GET"])
@permission_classes((IsAuthenticated, ))
def logout(request):
    Token.objects.filter(user=request.user.id).delete()
    return Response()


@api_view(["POST"])
@permission_classes((IsAuthenticated, ))
def reserve(request):
    reservation_request = serializers.ReservationSerializerWrite(data=request.data)
    if reservation_request.is_valid():
        with transaction.atomic():
            # find optimal accommodation
            accommodation = optimize.find_optimal_accommodation(
                reservation=reservation_request,
                now=datetime.datetime.now().date()
            )
            if accommodation is None:
                return errors.error_response(Error.NO_FREE_ACCOMMODATION)

            # make reservation
            reservation = reservation_request.save(
                user=request.user,
                accommodation=accommodation
            )

            # business logic checks:

            error: Error = None

            # correct capacity
            num_occupants = reservation.adults + sum(children.number for children in reservation.children.all())
            if not accommodation.type.capacity_min <= num_occupants <= accommodation.type.capacity_max:
                error = Error.WRONG_OCCUPANT_NUMBER

            # valid date interval
            if reservation.date_start > reservation.date_end:
                error = Error.INVALID_INTERVAL

            # reservation is during open time
            year = reservation.date_start.year
            if reservation.date_start.year != reservation.date_end.year\
                    or reservation.date_start < date(year, 5, 1)\
                    or reservation.date_end > date(year, 9, 30):
                error = Error.CLOSED

            if error is not None:
                reservation.delete()
                return errors.error_response(error)
            else:
                return Response(accommodation.id)
    return errors.serializer_error_response(reservation_request.errors)


@api_view(["POST"])
def register(request):
    serializer = serializers.UserSerializer(data=request.data)

    if serializer.is_valid():
        if len(models.User.objects.filter(email=serializer.validated_data["email"])):
            return errors.error_response(Error.USER_KEY_AREADY_EXISTS)

        key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20))

        user = serializer.save(
            email_confirm_key=key
        )

        url = request.build_absolute_uri(reverse("confirm-email", args=[key]))
        send_mail(
            subject="Potvrdite svoj email",
            message=f"Kliknite na sljedeću poveznicu kako biste potvrdili email: {url}",
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[user.email]
        )

        return Response()
    else:
        return errors.serializer_error_response(serializer.errors)


@api_view(["GET"])
def confirm_email(request, key):
    users = models.User.objects.filter(email_confirm_key=key)
    if not len(users):
        return errors.error_response(Error.INVALID_CONFIRM_LINK)
    users.update(email_confirmed=True)
    return Response()


@api_view(["GET"])
def test_view(request):
    send_mail(
        subject="hey",
        message="radi sve",
        from_email= settings.EMAIL_HOST_USER,
        recipient_list=["dsandi333@gmail.com"]
    )

    if not request.user.is_anonymous:
        return Response("hi!")
    else:
        return Response("nooope!")
