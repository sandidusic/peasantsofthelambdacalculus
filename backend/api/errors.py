from rest_framework.response import Response
from rest_framework import status


def error_response(error, aditional_info=""):
    code, message = error
    return Response(
        {
            "code": code,
            "message": message,
            "aditional_info": aditional_info,
        },
        status=status.HTTP_400_BAD_REQUEST
    )


def serializer_error_response(serializer_error):
    return error_response(
        Error.INVALID_DATA_FORMAT,
        aditional_info=str(serializer_error)
    )


class Error:
    INVALID_DATA_FORMAT = (0, "Invalid format of the request data")
    NO_FREE_ACCOMMODATION = (1, "Nije moguće provesti rezervaciju jer su sve smještajne jedinice tog tipa rezervirane u dano doba")
    WRONG_OCCUPANT_NUMBER = (2, "Željeni broj ljudi ne zadovoljava kapacitet tog tipa smještajne jedinice")
    INVALID_INTERVAL = (3, "Početni datum je nakon završnog datuma")
    CLOSED = (4, "Zatvoreni smo u to doba")
    EMAIL_NOT_CONFIRMED = (5, "Prijava nije moguća pošto email nije potvrđen")
    USER_KEY_AREADY_EXISTS = (6, "Registracija nije moguća pošto je ovaj email već iskorišten")
    INVALID_CONFIRM_LINK = (7, "Netočna poveznica za potvrdu registracije")
