from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django_countries.fields import CountryField
import enum
from django.utils.translation import gettext_lazy as _
from django.apps import AppConfig


def enum_to_choices(enum):
    return [(tag.name, tag.value) for tag in enum]


class ApartmentView(enum.Enum):
    PARK = "park"
    SEA = "more"


class ApartmentType(enum.Enum):
    APARTMENT = "apartman"
    ROOM = "soba"


class AgeClass(enum.Enum):
    AGE_0_1 = "0-1"
    AGE_2_7 = "2-7"
    AGE_8_14 = "8-14"


class AccommodationType(models.Model):
    id = models.AutoField(primary_key=True, verbose_name=_('oznaka'))
    view = models.CharField(max_length=32, choices=enum_to_choices(ApartmentView), verbose_name=_('pogled'))
    type = models.CharField(max_length=32, choices=enum_to_choices(ApartmentType), verbose_name=_('tip'))
    capacity_min = models.SmallIntegerField(verbose_name=_('minimalni kapacitet'))
    capacity_max = models.SmallIntegerField(verbose_name=_('maksimalni kapacitet'))

    class Meta:
        verbose_name = _('tip smještajne jedinice')
        verbose_name_plural = _('tipovi smještajnih jedinica')


class Accommodation(models.Model):
    id = models.CharField(max_length=4, primary_key=True, verbose_name='oznaka')
    building = models.CharField(max_length=1, verbose_name='zgrada')
    floor = models.SmallIntegerField(verbose_name='kat')
    description = models.TextField(blank=True, verbose_name='opis')
    image = models.ImageField(blank=True, verbose_name='slika')
    thumbnail = models.ImageField(default="thumbnail.jpg", verbose_name='thumbnail')
    type = models.ForeignKey(AccommodationType, models.CASCADE, related_name="accommodations", verbose_name='tip')

    class Meta:
        verbose_name = _('smještajna jedinica')
        verbose_name_plural = _('smještajne jedinice')

    def days_reserved(self):
        # django query not possible since sqlite stores dates as text -.-
        return sum(
            (reservation.date_end - reservation.date_start).days + 1
            for reservation in self.reservations.all()
        )
    days_reserved.short_description = "Rezervirano (dani)"

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # add a placeholder description if missing
        if not self.description:
            self.description = f"{self.type.get_type_display()} na {self.floor + 1}. katu {self.building} zgrade, s pogledom na {self.type.get_view_display()}"

        super().save(force_insert, force_update, using, update_fields)


@receiver(post_save, sender=Accommodation)
def create_thumbnail(sender, instance: Accommodation, created, raw, using, update_fields, **kwargs):
    if instance.image:
        from PIL import Image
        import os

        thumbnail_dir = "thumbnail"

        image_dir = os.path.join(settings.MEDIA_ROOT, instance.image.name)
        im = Image.open(image_dir)
        im.thumbnail((200, 200))
        _, ext = os.path.splitext(image_dir)
        thumbnail_name = os.path.join(thumbnail_dir, instance.pk + ext)
        im.save(os.path.join(settings.MEDIA_ROOT, thumbnail_name))
        Accommodation.objects.filter(pk=instance.pk).update(thumbnail=thumbnail_name)


class Reservation(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='korisnik')
    accommodation = models.ForeignKey(Accommodation, on_delete=models.CASCADE, related_name="reservations", verbose_name='smještajna jedinica')
    adults = models.IntegerField(verbose_name='odrasli')
    date_start = models.DateField(verbose_name='datum početka')
    date_end = models.DateField(verbose_name='datum kraja')
    parking = models.BooleanField(verbose_name='parking')
    wifi = models.BooleanField(verbose_name='wifi')
    tv = models.BooleanField(verbose_name='televizija')

    class Meta:
        verbose_name = _('rezervacija')
        verbose_name_plural = _('rezervacije')


class Children(models.Model):
    reservation = models.ForeignKey(Reservation, on_delete=models.CASCADE, related_name="children")
    number = models.IntegerField()
    age_class = models.CharField(max_length=32, choices=enum_to_choices(AgeClass))

    class Meta:
        unique_together = (("age_class", "reservation"),)


class User(AbstractUser):
    address = models.CharField(max_length=256, verbose_name='adresa')
    city = models.CharField(max_length=32, verbose_name='grad')
    country = CountryField(verbose_name='država')
    telephone = models.CharField(max_length=32, blank=True, verbose_name='telefon')
    email_confirmed = models.BooleanField(default=False, verbose_name="email potvrđen")
    email_confirm_key = models.CharField(max_length=32, blank=True)

    class Meta:
        verbose_name = _('korisnik')
        verbose_name_plural = _('korisnici')
