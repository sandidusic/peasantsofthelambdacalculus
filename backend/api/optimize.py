from django.db.models import Q
from datetime import date
import typing

from . import serializers
from . import models


def find_optimal_accommodation(
        reservation: serializers.ReservationSerializerWrite,
        now: date
) -> typing.Optional[models.Accommodation]:
    candidates = list(models.Accommodation.objects.filter(
        type=reservation.validated_data["accommodation_type"],
    ).filter(
        # next two filter eliminate all accommodations with reservations that overlap with the desired one
        Q(reservations__date_end__lt=reservation.validated_data["date_start"]) |
        Q(reservations__isnull=True)  # either they have don't overlap or there aren't any
    ).filter(
        Q(reservations__date_start__gt=reservation.validated_data["date_end"]) |
        Q(reservations__isnull=True)
    ))

    def sorting_key(
            accommodation: models.Accommodation
    ) -> int:
        reservations_before = accommodation.reservations.filter(
            date_start__gte=now,
            date_start__lt=reservation.validated_data["date_start"]
        ).order_by("-date_start")

        if len(reservations_before):
            return (reservation.validated_data["date_start"] - reservations_before[0].date_end).days
        else:
            return 0

    if candidates:
        return min(candidates, key=sorting_key)
    else:
        return None



