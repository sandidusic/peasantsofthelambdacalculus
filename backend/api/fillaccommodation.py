from api.models import *
from os import listdir
from os.path import join
from django.db import transaction


def fill():
    AccommodationType.objects.all().delete()
    Accommodation.objects.all().delete()

    types = [
        AccommodationType(
            id=1,
            view=ApartmentView.SEA.name,
            type=ApartmentType.APARTMENT.name,
            capacity_min=2,
            capacity_max=4
        ),
        AccommodationType(
            id=2,
            view=ApartmentView.PARK.name,
            type=ApartmentType.APARTMENT.name,
            capacity_min=2,
            capacity_max=4
        ),
        AccommodationType(
            id=3,
            view=ApartmentView.SEA.name,
            type=ApartmentType.APARTMENT.name,
            capacity_min=6,
            capacity_max=8
        ),
        AccommodationType(
            id=4,
            view=ApartmentView.PARK.name,
            type=ApartmentType.APARTMENT.name,
            capacity_min=6,
            capacity_max=8
        ),
        AccommodationType(
            id=5,
            view=ApartmentView.SEA.name,
            type=ApartmentType.ROOM.name,
            capacity_min=2,
            capacity_max=3
        ),
        AccommodationType(
            id=6,
            view=ApartmentView.PARK.name,
            type=ApartmentType.ROOM.name,
            capacity_min=2,
            capacity_max=3
        ),
    ]
    AccommodationType.objects.bulk_create(types)

    hotel = [
        [
            [1, 1, 2, 2],
            [1, 1, 2, 2],
        ],
        [
            [1, 1, 2, 2],
            [1, 1, 2, 2],
        ],
        [
            [3, 4],
            [3, 4],
        ],
        [
            [3, 4],
            [5]*4 + [6]*4,
        ],
    ]

    rooms = []
    apartments = []

    for building, floors in zip("ABCD", hotel):
        for floor, accommodations in enumerate(floors):
            for acc_num, acc_type in enumerate(accommodations):
                acc = Accommodation(
                    id=f"{building}{floor + 1}-{acc_num + 1}",
                    building=building,
                    floor=floor,
                    type=AccommodationType.objects.get(pk=acc_type)
                )
                if acc.type.type == ApartmentType.ROOM:
                    rooms.append(acc)
                else:
                    apartments.append(acc)

    def images(image_dir):
        from backend.settings import MEDIA_ROOT
        return (join(image_dir, filename) for filename in listdir(join(MEDIA_ROOT, image_dir)))

    for acc_list, image_dir in [(rooms, "room"), (apartments, "apartment")]:
        for acc, image in zip(acc_list, images(image_dir)):
            acc.image = image

    # can't use bulk_create because it doesn't call the save override nor the post_save signal
    with transaction.atomic():
        for acc in apartments + rooms:
            acc.save()
