package hotel.opp.fer.hr.pictureretrofit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView listView = findViewById(R.id.proagation_list);

        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://jsonplaceholder.typicode.com")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        OnlineClient client = retrofit.create(OnlineClient.class);
        Call<List<PictureStorage>> call = client.photosFromAlbum("photos");

        call.enqueue(new Callback<List<PictureStorage>>() {
            @Override
            public void onResponse(Call<List<PictureStorage>> call, Response<List<PictureStorage>> response) {
                List<PictureStorage> pictures = response.body();

                listView.setAdapter(new PictureListAdapter(MainActivity.this, pictures));
            }

            @Override
            public void onFailure(Call<List<PictureStorage>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "error :(", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
