package hotel.opp.fer.hr.pictureretrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

interface OnlineClient {

    @GET("/{part}")
    Call<List<PictureStorage>> photosFromAlbum(@Path("part") String apiPart);

}
