package hotel.opp.fer.hr.pictureretrofit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class PictureListAdapter extends ArrayAdapter<PictureStorage> {

    private Context context;
    private List<PictureStorage> pictures;

    public PictureListAdapter(Context context, List<PictureStorage> pictures) {
        super(context, R.layout.list_item_pagination, pictures);

        this.context = context;
        this.pictures = pictures;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PictureStorage item = pictures.get(position);

        if (row == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.list_item_pagination, parent, false);
        }

        TextView textView = (TextView) row.findViewById(R.id.list_item_pagination_text);
        ImageView imageView = (ImageView) row.findViewById(R.id.list_item_image);

        textView.setText(item.getTitle());
        Picasso.get().load(item.getUrl()).into(imageView);

        return row;
    }
}