package opp.fer.hr.hotel_booking.forms;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

public class Accommodation implements Serializable, Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    public final static Parcelable.Creator<Accommodation> CREATOR = new Creator<Accommodation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Accommodation createFromParcel(Parcel in) {
            return new Accommodation(in);
        }

        public Accommodation[] newArray(int size) {
            return (new Accommodation[size]);
        }

    };
    @SerializedName("building")
    @Expose
    private String building;
    @SerializedName("floor")
    @Expose
    private Integer floor;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("type")
    @Expose
    private Integer type;
    private final static long serialVersionUID = -8978549252496554582L;
    @SerializedName("reservations")
    @Expose
    private List<ReservationTime> reservations = null;

    protected Accommodation(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.reservations, (ReservationTime.class.getClassLoader()));
        this.building = ((String) in.readValue((String.class.getClassLoader())));
        this.floor = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.thumbnail = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Accommodation() {
    }

    /**
     *
     * @param id
     * @param building
     * @param thumbnail
     * @param floor
     * @param description
     * @param image
     * @param type
     * @param reservations
     */
    public Accommodation(String id, List<ReservationTime> reservations, String building, Integer floor, String description, String image, String thumbnail, Integer type) {
        super();
        this.id = id;
        this.reservations = reservations;
        this.building = building;
        this.floor = floor;
        this.description = description;
        this.image = image;
        this.thumbnail = thumbnail;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ReservationTime> getReservations() {
        return reservations;
    }

    public void setReservations(List<ReservationTime> reservations) {
        this.reservations = reservations;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(building).append(thumbnail).append(floor).append(description).append(image).append(type).append(reservations).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Accommodation) == false) {
            return false;
        }
        Accommodation rhs = ((Accommodation) other);
        return new EqualsBuilder().append(id, rhs.id).append(building, rhs.building).append(thumbnail, rhs.thumbnail).append(floor, rhs.floor).append(description, rhs.description).append(image, rhs.image).append(type, rhs.type).append(reservations, rhs.reservations).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeList(reservations);
        dest.writeValue(building);
        dest.writeValue(floor);
        dest.writeValue(description);
        dest.writeValue(image);
        dest.writeValue(thumbnail);
        dest.writeValue(type);
    }

    public int describeContents() {
        return 0;
    }

}