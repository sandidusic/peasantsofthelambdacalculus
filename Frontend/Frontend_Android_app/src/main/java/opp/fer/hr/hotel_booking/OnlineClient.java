package opp.fer.hr.hotel_booking;

import okhttp3.*;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class OnlineClient {

    private static String TOKEN = null;

    private static OnlineAPI client;
    private static final String BASE_URL = "https://sandidusic.pythonanywhere.com";
    private static ClientSecurity currentSecurity;
    private static Retrofit retrofit;

    private static void CreateClient() {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create());

        retrofit = builder.build();
        client = retrofit.create(OnlineAPI.class);

    }

    private static void CreateSecureClient() {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create()).client(createInterceptorBuilder().build());

        retrofit = builder.build();
        client = retrofit.create(OnlineAPI.class);
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static OnlineAPI get(ClientSecurity level) {
        if (client == null || currentSecurity == null) {
            if (level == ClientSecurity.PUBLIC)
                CreateClient();
            else
                CreateSecureClient();

            currentSecurity = level;
        }

        if (currentSecurity != level) {
            currentSecurity = null;
            return get(level);
        }

        return client;
    }

    public static class NullOnEmptyConverterFactory extends Converter.Factory {
        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
            final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
            return new Converter<ResponseBody, Object>() {
                @Override
                public Object convert(ResponseBody body) throws IOException {
                    if (body.contentLength() == 0) return null;
                    return delegate.convert(body);
                }
            };
        }
    }

    private static OkHttpClient.Builder createInterceptorBuilder() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Authorization", "Token " +
                        TOKEN).build();
                return chain.proceed(request);
            }
        });

        return httpClient;
    }

    public enum ClientSecurity {
        SECURE,
        PUBLIC
    }

    public static String getTOKEN() {
        return TOKEN;
    }

    public static void setTOKEN(String TOKEN) {
        OnlineClient.TOKEN = TOKEN;
    }
}
