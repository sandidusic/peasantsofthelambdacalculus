package opp.fer.hr.hotel_booking;

import java.util.HashMap;
import java.util.Map;

public class EnumDictionary {

    private static Map<String, String> dictionary;

    class RoomID {
        static final short ROOM = 0, APARTMENT = 1;
    }

    class ViewID {
        static final short PARK = 0, SEA = 1;
    }

    class AgeClass {
        static final String C0_1 = "AGE_0_1", C2_7 = "AGE_2_7", C8_14 = "AGE_8_14";
    }

    public EnumDictionary() {
        construct();
    }

    private static void construct() {
        dictionary = new HashMap<>();

        add("SEA", "More");
        add("APARTMENT", "Apartman");
        add("PARK", "Park");
        add("ROOM", "Soba");
    }

    public static Map<String, String> get() {
        if (dictionary == null) {
            construct();
        }

        return dictionary;
    }

    public static void add(String code, String value) {
        Map<String, String> dic = get();
        dic.put(code, value);
    }
}
