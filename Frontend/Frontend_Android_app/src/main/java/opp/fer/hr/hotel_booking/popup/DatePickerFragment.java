package opp.fer.hr.hotel_booking.popup;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import opp.fer.hr.hotel_booking.ISO8601;

import java.util.Calendar;

public class DatePickerFragment extends android.support.v4.app.DialogFragment implements DatePickerDialog.OnDateSetListener {

    public static final String DATE_PICKER_ARG = "opp.fer.hr.hotel_booking_date_picker_arg";

    int mViewId;

    public static DatePickerFragment newInstance(int mViewId) {

        Bundle args = new Bundle();
        args.putInt(DATE_PICKER_ARG, mViewId);

        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        mViewId = getArguments().getInt(DATE_PICKER_ARG);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        EditText text = getActivity().findViewById(mViewId);
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day);
        text.setText(ISO8601.fromCalendar(cal));
    }
}
