package opp.fer.hr.hotel_booking.forms;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class ReservationTime implements Serializable, Parcelable {

    public final static Parcelable.Creator<Reservation> CREATOR = new Creator<Reservation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Reservation createFromParcel(Parcel in) {
            return new Reservation(in);
        }

        public Reservation[] newArray(int size) {
            return (new Reservation[size]);
        }

    };
    private final static long serialVersionUID = 1057808653131877170L;
    @SerializedName("date_start")
    @Expose
    private String dateStart;
    @SerializedName("date_end")
    @Expose
    private String dateEnd;

    protected ReservationTime(Parcel in) {
        this.dateStart = ((String) in.readValue((String.class.getClassLoader())));
        this.dateEnd = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public ReservationTime() {
    }

    /**
     * @param dateEnd
     * @param dateStart
     */
    public ReservationTime(String dateStart, String dateEnd) {
        super();
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dateEnd).append(dateStart).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ReservationTime) == false) {
            return false;
        }
        ReservationTime rhs = ((ReservationTime) other);
        return new EqualsBuilder().append(dateEnd, rhs.dateEnd).append(dateStart, rhs.dateStart).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(dateStart);
        dest.writeValue(dateEnd);
    }

    public int describeContents() {
        return 0;
    }

}
