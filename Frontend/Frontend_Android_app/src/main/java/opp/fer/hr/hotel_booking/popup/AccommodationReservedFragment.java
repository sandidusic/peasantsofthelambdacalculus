package opp.fer.hr.hotel_booking.popup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnRangeSelectedListener;
import opp.fer.hr.hotel_booking.R;
import opp.fer.hr.hotel_booking.forms.Accommodation;

import java.util.Calendar;
import java.util.List;

public class AccommodationReservedFragment extends DialogFragment {

    public static final String ACCOMMODATION_RESERVED_ARG = "opp.fer.hr.hotel_booking_accommodation_reserved_arg";

    private MaterialCalendarView calendarView;

    private Calendar nextYear;
    private Calendar lastMonth;

    private Accommodation accommodation;

    public static AccommodationReservedFragment newInstance(Accommodation accommodation) {

        Bundle args = new Bundle();
        args.putParcelable(ACCOMMODATION_RESERVED_ARG, accommodation);
        AccommodationReservedFragment fragment = new AccommodationReservedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_reserved_dates, container);
    }


    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        accommodation = getArguments().getParcelable(ACCOMMODATION_RESERVED_ARG);

        nextYear = Calendar.getInstance();
        nextYear.set(today.year + 1, today.month, today.monthDay);
        lastMonth = Calendar.getInstance();
        lastMonth.set(today.year, today.month - 1, today.monthDay);

        calendarView = v.findViewById(R.id.calendarView);
        calendarView.addDecorator(new EventDecorator(getContext(), accommodation));

        int minYear = 0;
        int maxYear = 0;
        if (today.month > 9) {
            minYear = today.year + 1;
            maxYear = today.year + 1;
        } else if (today.month < 5) {
            minYear = today.year;
            maxYear = today.year;
        }


        calendarView.state().edit()
                .setMinimumDate(CalendarDay.from(minYear,
                        4, lastMonth.getTime().getDate()))
                .setMaximumDate(CalendarDay.from(maxYear,
                        8, nextYear.getTime().getDate()))
                .commit();


        calendarView.setOnRangeSelectedListener(new OnRangeSelectedListener() {
            @Override
            public void onRangeSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull List<CalendarDay> list) {

            }
        });

    }


}
