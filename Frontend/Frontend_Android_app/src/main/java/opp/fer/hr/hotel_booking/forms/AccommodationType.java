package opp.fer.hr.hotel_booking.forms;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import opp.fer.hr.hotel_booking.EnumDictionary;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class AccommodationType implements Serializable, Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("accommodations")
    @Expose
    private List<String> accommodations = null;
    @SerializedName("view")
    @Expose
    private String view;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("capacity_min")
    @Expose
    private Integer capacityMin;
    @SerializedName("capacity_max")
    @Expose
    private Integer capacityMax;
    public final static Parcelable.Creator<AccommodationType> CREATOR = new Creator<AccommodationType>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AccommodationType createFromParcel(Parcel in) {
            return new AccommodationType(in);
        }

        public AccommodationType[] newArray(int size) {
            return (new AccommodationType[size]);
        }

    };

    private final static long serialVersionUID = 4455902855222772589L;

    protected AccommodationType(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.accommodations, (java.lang.String.class.getClassLoader()));
        this.view = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.capacityMin = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.capacityMax = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public AccommodationType() {
    }

    /**
     *
     * @param id
     * @param accommodations
     * @param capacityMax
     * @param view
     * @param type
     * @param capacityMin
     */
    public AccommodationType(Integer id, List<String> accommodations, String view, String type, Integer capacityMin, Integer capacityMax) {
        super();
        this.id = id;
        this.accommodations = accommodations;
        this.view = view;
        this.type = type;
        this.capacityMin = capacityMin;
        this.capacityMax = capacityMax;
    }

    public String getViewTranslated() {
        Map<String, String> dictionary = EnumDictionary.get();
        return dictionary.get(getView());
    }

    public String getTypeTranslated() {
        Map<String, String> dictionary = EnumDictionary.get();
        return dictionary.get(getType());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getAccommodations() {
        return accommodations;
    }

    public void setAccommodations(List<String> accommodations) {
        this.accommodations = accommodations;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCapacityMin() {
        return capacityMin;
    }

    public void setCapacityMin(Integer capacityMin) {
        this.capacityMin = capacityMin;
    }

    public Integer getCapacityMax() {
        return capacityMax;
    }

    public void setCapacityMax(Integer capacityMax) {
        this.capacityMax = capacityMax;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(accommodations).append(capacityMax).append(view).append(type).append(capacityMin).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AccommodationType) == false) {
            return false;
        }
        AccommodationType rhs = ((AccommodationType) other);
        return new EqualsBuilder().append(id, rhs.id).append(accommodations, rhs.accommodations).append(capacityMax, rhs.capacityMax).append(view, rhs.view).append(type, rhs.type).append(capacityMin, rhs.capacityMin).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeList(accommodations);
        dest.writeValue(view);
        dest.writeValue(type);
        dest.writeValue(capacityMin);
        dest.writeValue(capacityMax);
    }

    public int describeContents() {
        return 0;
    }

}
