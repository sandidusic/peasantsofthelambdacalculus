package opp.fer.hr.hotel_booking.popup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.gson.Gson;
import opp.fer.hr.hotel_booking.OnlineAPI;
import opp.fer.hr.hotel_booking.OnlineClient;
import opp.fer.hr.hotel_booking.R;
import opp.fer.hr.hotel_booking.forms.LoginInformation;
import opp.fer.hr.hotel_booking.forms.TokenOrError;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;

public class LoginDialogFragment extends DialogFragment {

    private EditText mEmailField;
    private EditText mPasswordField;
    private Button mConfirmButton;

    private OnlineAPI client;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_dialog, container);
    }


    @Override

    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(v, savedInstanceState);

        client = OnlineClient.get(OnlineClient.ClientSecurity.PUBLIC);

        mEmailField = v.findViewById(R.id.email_login_edit_field);
        mPasswordField = v.findViewById(R.id.password_login_edit_field);
        mConfirmButton = v.findViewById(R.id.button_login_confirm);

        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final LoginInformation information = new LoginInformation(mEmailField.getText().toString(),
                        mPasswordField.getText().toString());

                Thread retrofitCallThreadLogin = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final Call<TokenOrError> call = client.postLogin(information);
                        try {
                            Response<TokenOrError> response = call.execute();

                            if (response.isSuccessful()) {
                                OnlineClient.setTOKEN(response.body().getToken());

                                showError("Uspješno prijavljivanje!");
                            } else if (response.code() >= 400 && response.code() != 500) {
                                Gson gson = new Gson();
                                final TokenOrError error = gson.fromJson(response.errorBody().charStream()
                                        , TokenOrError.class);

                                List<String> nonFieldError = error.getNonFieldErrors();
                                List<String> nonPasswordError = error.getPassword();
                                List<String> nonUsernameError = error.getUsername();

                                String displayString = null;
                                if (nonFieldError != null)
                                    displayString = "Email ili šifra koju ste unijeli nije ispravna";
                                else if (nonPasswordError != null)
                                    displayString = "Unesite šifru";
                                else if (nonUsernameError != null)
                                    displayString = "Unesite email";
                                else {
                                    showError("email nije potvrđen");
                                }

                                final String displayIntoLambda = displayString;
                                showError(displayIntoLambda);
                            } else {
                                showError("Greška u spajanju na internet");
                            }

                        } catch (IOException e) {
                            showError("Greška u spajanju na internet");
                        }

                    }
                });

                retrofitCallThreadLogin.start();
                try {
                    retrofitCallThreadLogin.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dismiss();
            }
        });
    }

    private void showError(final String s) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
