package opp.fer.hr.hotel_booking;

import opp.fer.hr.hotel_booking.forms.*;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

import java.util.List;

public interface OnlineAPI {

    @GET("/api/accommodations/?format=json")
    Call<List<Accommodation>> getAccommodations();

    @GET("/api/types/?format=json")
    Call<List<AccommodationType>> getAccommodationTypes();

    @POST("/api/login")
    Call<TokenOrError> postLogin(@Body LoginInformation information);

    //Could handle errors here as well
    @POST("/api/reserve")
    Call<ErrorMessage> postReservation(@Body Reservation reservation);

    @POST("/api/register")
    Call<ErrorMessage> postRegistration(@Body RegistryForm registryForm);
}
