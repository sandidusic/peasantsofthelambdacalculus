package opp.fer.hr.hotel_booking.forms;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class ErrorMessage implements Serializable, Parcelable {

    public final static Parcelable.Creator<ErrorMessage> CREATOR = new Creator<ErrorMessage>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ErrorMessage createFromParcel(Parcel in) {
            return new ErrorMessage(in);
        }

        public ErrorMessage[] newArray(int size) {
            return (new ErrorMessage[size]);
        }

    };
    private final static long serialVersionUID = -5989567442432122302L;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("aditional_info")
    @Expose
    private String aditionalInfo;

    protected ErrorMessage(Parcel in) {
        this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.aditionalInfo = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public ErrorMessage() {
    }

    /**
     * @param message
     * @param aditionalInfo
     * @param code
     */
    public ErrorMessage(Integer code, String message, String aditionalInfo) {
        super();
        this.code = code;
        this.message = message;
        this.aditionalInfo = aditionalInfo;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAditionalInfo() {
        return aditionalInfo;
    }

    public void setAditionalInfo(String aditionalInfo) {
        this.aditionalInfo = aditionalInfo;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(message).append(aditionalInfo).append(code).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ErrorMessage) == false) {
            return false;
        }
        ErrorMessage rhs = ((ErrorMessage) other);
        return new EqualsBuilder().append(message, rhs.message).append(aditionalInfo, rhs.aditionalInfo).append(code, rhs.code).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeValue(aditionalInfo);
    }

    public int describeContents() {
        return 0;
    }

}
