package opp.fer.hr.hotel_booking;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import opp.fer.hr.hotel_booking.forms.Accommodation;
import opp.fer.hr.hotel_booking.forms.AccommodationType;
import opp.fer.hr.hotel_booking.popup.AccommodationReservedFragment;

public class ApartmentFragment extends Fragment {

    public static final String ARG_APARTMENT_ID = "arg_apartment_id";

    private final String REGISTER_TEXT = "Registriraj se";
    private final String RENT_TEXT = "Iznajmi ovakav tip";

    private int width;

    public TextView mTextViewType;
    private TextView mApartmentTitle;
    private TextView mApartmentDetail;
    private Button mButton;
    private Button mFragmentDateButton;
    public TextView mTextViewCapacityMin;
    public TextView mTextViewCapacityMax;
    public TextView mTextViewLook;
    private Accommodation mAccommodation;
    private AccommodationType mType;
    private ImageView mImageView;

    public static ApartmentFragment newInstance(String apartmentId) { //change hashcode to UUID
        Bundle args = new Bundle();
        args.putSerializable(ARG_APARTMENT_ID, apartmentId);

        ApartmentFragment apartmentFragment = new ApartmentFragment();
        apartmentFragment.setArguments(args);

        return apartmentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String apartmentID = (String) getArguments().getSerializable(ARG_APARTMENT_ID);
        mAccommodation = AccommodationStorage.get(getActivity()).getAccommodation(apartmentID);
        mType = AccommodationStorage.get(getContext()).getAccommodationType(mAccommodation.getType());

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = 300 + size.x/2;
    }

    @Override
    public void onResume() {
        super.onResume();
        UpdateButton();

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = 300 + size.x/2;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_apartment, container, false);

        mApartmentTitle = v.findViewById(R.id.apartment_title_specific);
        mApartmentDetail = v.findViewById(R.id.detail_title_specific);
        mButton = v.findViewById(R.id.generic_apartment_button);
        mFragmentDateButton = v.findViewById(R.id.fragment_reserved_show_button);
        mImageView = v.findViewById(R.id.image_apartment_view);

        mTextViewType = v.findViewById(R.id.apartment_type_room_list_specific);
        mTextViewCapacityMin = v.findViewById(R.id.apartment_capacity_list_min_specific);
        mTextViewCapacityMax = v.findViewById(R.id.apartment_capacity_list_max_specific);
        mTextViewLook = v.findViewById(R.id.apartment_look_at_list_specific);

        Picasso.get().load(mAccommodation.getImage()).resize(width, width ).into(mImageView);
        UpdateButton();

        mApartmentTitle.setText(mAccommodation.getId());
        mTextViewType.setText(mType.getTypeTranslated());
        mTextViewCapacityMin.setText(Integer.toString(mType.getCapacityMin()));
        mTextViewCapacityMax.setText(Integer.toString(mType.getCapacityMax()));
        mTextViewLook.setText(mType.getViewTranslated());

        mApartmentDetail.setText(mAccommodation.getDescription());

        mFragmentDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = AccommodationReservedFragment.newInstance(mAccommodation);
                newFragment.show(getFragmentManager(), "Prikaz zauzetih termina");
            }
        });
        return v;
    }

    private void UpdateButton() {
        if (OnlineClient.getTOKEN() == null) {
            mButton.setText(REGISTER_TEXT);
            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(RegisterActivity.makeIntent(getContext()));
                }
            });
        } else {
            mButton.setText(RENT_TEXT);
            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(ReserveActivity.makeIntent(mAccommodation.getId(), getContext()));
                }
            });
        }
    }
}
