package opp.fer.hr.hotel_booking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class RegisterActivity extends SingleFragmentActivity {

    private static final String EXTRA_REGISTER_ID = "practice.fer.hr.hotel_booking.registerID";

    public static Intent makeIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, RegisterActivity.class);
        return intent;
    }


    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Fragment AllocateFragment() {
        return new RegisterFragment();
    }
}
