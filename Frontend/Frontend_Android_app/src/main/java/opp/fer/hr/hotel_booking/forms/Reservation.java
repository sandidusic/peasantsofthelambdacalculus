package opp.fer.hr.hotel_booking.forms;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

public class Reservation implements Serializable, Parcelable
{

    @SerializedName("accommodation_type")
    @Expose
    private Integer accommodationType;
    private final static long serialVersionUID = 7903442451295118865L;
    @SerializedName("children")
    @Expose
    private List<Child> children = null;
    @SerializedName("date_start")
    @Expose
    private String dateStart;
    @SerializedName("date_end")
    @Expose
    private String dateEnd;
    @SerializedName("parking")
    @Expose
    private Boolean parking;
    @SerializedName("wifi")
    @Expose
    private Boolean wifi;
    @SerializedName("tv")
    @Expose
    private Boolean tv;
    public final static Parcelable.Creator<Reservation> CREATOR = new Creator<Reservation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Reservation createFromParcel(Parcel in) {
            return new Reservation(in);
        }

        public Reservation[] newArray(int size) {
            return (new Reservation[size]);
        }

    }
            ;
    @SerializedName("adults")
    @Expose
    private Integer adults;

    protected Reservation(Parcel in) {
        this.accommodationType = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.adults = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.children, (opp.fer.hr.hotel_booking.forms.Child.class.getClassLoader()));
        this.dateStart = ((String) in.readValue((String.class.getClassLoader())));
        this.dateEnd = ((String) in.readValue((String.class.getClassLoader())));
        this.parking = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.wifi = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.tv = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Reservation() {
    }

    /**
     *
     * @param parking
     * @param wifi
     * @param tv
     * @param accommodationType
     * @param adults
     * @param children
     * @param dateEnd
     * @param dateStart
     */
    public Reservation(Integer accommodationType, Integer adults, List<Child> children, String dateStart, String dateEnd, Boolean parking, Boolean wifi, Boolean tv) {
        super();
        this.accommodationType = accommodationType;
        this.adults = adults;
        this.children = children;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.parking = parking;
        this.wifi = wifi;
        this.tv = tv;
    }

    public Integer getAccommodationType() {
        return accommodationType;
    }

    public void setAccommodationType(Integer accommodationType) {
        this.accommodationType = accommodationType;
    }

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Boolean getParking() {
        return parking;
    }

    public void setParking(Boolean parking) {
        this.parking = parking;
    }

    public Boolean getWifi() {
        return wifi;
    }

    public void setWifi(Boolean wifi) {
        this.wifi = wifi;
    }

    public Boolean getTv() {
        return tv;
    }

    public void setTv(Boolean tv) {
        this.tv = tv;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(parking).append(wifi).append(tv).append(accommodationType).append(adults).append(children).append(dateEnd).append(dateStart).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Reservation) == false) {
            return false;
        }
        Reservation rhs = ((Reservation) other);
        return new EqualsBuilder().append(parking, rhs.parking).append(wifi, rhs.wifi).append(tv, rhs.tv).append(accommodationType, rhs.accommodationType).append(adults, rhs.adults).append(children, rhs.children).append(dateEnd, rhs.dateEnd).append(dateStart, rhs.dateStart).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(accommodationType);
        dest.writeValue(adults);
        dest.writeList(children);
        dest.writeValue(dateStart);
        dest.writeValue(dateEnd);
        dest.writeValue(parking);
        dest.writeValue(wifi);
        dest.writeValue(tv);
    }

    public int describeContents() {
        return 0;
    }

}