package opp.fer.hr.hotel_booking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class ApartmentActivity extends SingleFragmentActivity {

    private static final String EXTRA_APARTMENT_ID = "practice.fer.hr.hotel_booking.apartmentID";

    public static Intent makeIntent(Context packageContext, String hashCode) { //hash needs to be replaced with UUID
        Intent intent = new Intent(packageContext, ApartmentActivity.class);
        intent.putExtra(EXTRA_APARTMENT_ID, hashCode);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AccommodationStorage.get(getApplicationContext());
    }

    @Override
    public Fragment AllocateFragment() {
        return createFragment();
    }

    protected ApartmentFragment createFragment() {
        String activityUUID = (String) getIntent().getSerializableExtra(EXTRA_APARTMENT_ID);
        return ApartmentFragment.newInstance(activityUUID);
    }

}
