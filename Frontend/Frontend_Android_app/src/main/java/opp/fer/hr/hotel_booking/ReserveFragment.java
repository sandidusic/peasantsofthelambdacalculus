package opp.fer.hr.hotel_booking;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.google.gson.Gson;
import opp.fer.hr.hotel_booking.forms.Accommodation;
import opp.fer.hr.hotel_booking.forms.Child;
import opp.fer.hr.hotel_booking.forms.ErrorMessage;
import opp.fer.hr.hotel_booking.forms.Reservation;
import opp.fer.hr.hotel_booking.popup.ReserveAccommoadtionFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReserveFragment extends Fragment {

    public static final String ARG_RESERVE_ID = "arg_reserve_id"; //Not needed currently, no arguments

    private Calendar mCalendar;
    private Accommodation mAccommodation;

    private Button mDatePickerButton;

    private EditText mNumberOfAdults;
    private EditText mNumberOfChildren_1;
    private EditText mNumberOfChildren_7;
    private EditText mNumberOfChildren_14;

    private TextView mTextView1;
    private TextView mTextView2;

    private CheckBox mCheckBoxParking;
    private CheckBox mCheckBoxWifi;
    private CheckBox mCheckBoxTv;
    private RelativeLayout mLayoutParking;
    private RelativeLayout mLayoutWifi;
    private RelativeLayout mLayoutTv;

    private Button mButtonReserve;

    private Handler handler;

    private OnlineAPI client;

    public static ReserveFragment newInstance(Accommodation accommodation) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_RESERVE_ID, accommodation);

        ReserveFragment fragment = new ReserveFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        handler = new Handler();

        if(savedInstanceState != null)
        {
            savedInstanceState = savedInstanceState.getBundle("save_bundle");
            mAccommodation = savedInstanceState.getParcelable("accommodation");
            mTextView1.setText(savedInstanceState.getString("date1"));
            mTextView1.setText(savedInstanceState.getString("date2"));
            mNumberOfAdults.setText(savedInstanceState.getString("adult"));
            mNumberOfChildren_1.setText(savedInstanceState.getString("ch0_1"));
            mNumberOfChildren_7.setText(savedInstanceState.getString("ch2_7"));
            mNumberOfChildren_14.setText(savedInstanceState.getString("ch8_14"));
            mCheckBoxParking.setChecked(savedInstanceState.getBoolean("parking"));
            mCheckBoxWifi.setChecked(savedInstanceState.getBoolean("wifi"));
            mCheckBoxTv.setChecked(savedInstanceState.getBoolean("tv"));
        }

        mAccommodation = (Accommodation) getArguments().getSerializable(ARG_RESERVE_ID);
        mCalendar = Calendar.getInstance();
        client = OnlineClient.get(OnlineClient.ClientSecurity.SECURE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_reserve, container, false);

        mDatePickerButton = v.findViewById(R.id.date_picker_button);
        mNumberOfAdults = v.findViewById(R.id.adult_number_edit);
        mNumberOfChildren_1 = v.findViewById(R.id.children01_number_edit);
        mNumberOfChildren_7 = v.findViewById(R.id.children27_number_edit);
        mNumberOfChildren_14 = v.findViewById(R.id.children714_number_edit);
        mCheckBoxParking = v.findViewById(R.id.checkbox_parking);
        mCheckBoxWifi = v.findViewById(R.id.checkbox_wifi);
        mCheckBoxTv = v.findViewById(R.id.checkbox_tv);
        mButtonReserve = v.findViewById(R.id.reserve_confirm_button);
        mTextView1 = v.findViewById(R.id.date_reserve_text_view1);
        mTextView2 = v.findViewById(R.id.date_reserve_text_view2);
        mLayoutParking = v.findViewById(R.id.lin11);
        mLayoutWifi = v.findViewById(R.id.lin22);
        mLayoutTv = v.findViewById(R.id.lin33);

        mCheckBoxTv.setClickable(true);
        mCheckBoxWifi.setClickable(true);
        mCheckBoxParking.setClickable(true);

        mLayoutParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        v.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.click_checkbox));
                    }
                });
                boolean set = !mCheckBoxParking.isChecked();

                mCheckBoxParking.setChecked(set);
            }
        });

        mLayoutWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        v.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.click_checkbox));
                    }
                });
                boolean set = !mCheckBoxWifi.isChecked();

                mCheckBoxWifi.setChecked(set);
            }
        });

        mLayoutTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        v.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.click_checkbox));
                    }
                });
                boolean set = !mCheckBoxTv.isChecked();

                mCheckBoxTv.setChecked(set);
            }
        });

        mDatePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = ReserveAccommoadtionFragment.newInstance(R.id.date_reserve_text_view1
                        , R.id.date_reserve_text_view2, mAccommodation.getId());
                newFragment.show(getFragmentManager(), "Date Picker");
            }
        });

        mButtonReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Reservation reservation = createReservation();

                Call call = client.postReservation(reservation);
                final AccommodationStorage accommodationStorageReference = AccommodationStorage.get(getActivity());

                call.enqueue(new Callback<ErrorMessage>() {
                    @Override
                    public void onResponse(Call<ErrorMessage> call,
                                           Response<ErrorMessage> response) {

                        if (response.isSuccessful()) {
                            Toast.makeText(getContext(), "Rezervacija uspiješno poslana!", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        } else if (response.code() >= 400) {
                            Gson gson = new Gson();
                            ErrorMessage message = gson.fromJson(response.errorBody().charStream(), ErrorMessage.class);

                            switch (message.getCode()) {
                                case 1:
                                    Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_SHORT).show();
                                    break;
                                case 2:
                                    Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_SHORT).show();
                                    break;
                                case 3:
                                    Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_SHORT).show();
                                    break;
                                case 4:
                                    Toast.makeText(getContext(), message.getMessage(), Toast.LENGTH_SHORT).show();
                                    break;
                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<ErrorMessage> call, Throwable t) {
                        Toast.makeText(getContext(), "Greška u spajanju na internet", Toast.LENGTH_SHORT).show();

                    }
                });
            }
            }
        );

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        handler = new Handler();

        Bundle savedInstanceState = getArguments().getBundle("save_state");
        if(savedInstanceState != null) {
            mAccommodation = savedInstanceState.getParcelable("accommodation");
            mTextView1.setText(savedInstanceState.getString("date1"));
            mTextView1.setText(savedInstanceState.getString("date2"));
            mNumberOfAdults.setText(savedInstanceState.getString("adult"));
            mNumberOfChildren_1.setText(savedInstanceState.getString("ch0_1"));
            mNumberOfChildren_7.setText(savedInstanceState.getString("ch2_7"));
            mNumberOfChildren_14.setText(savedInstanceState.getString("ch8_14"));
            mCheckBoxParking.setChecked(savedInstanceState.getBoolean("parking"));
            mCheckBoxWifi.setChecked(savedInstanceState.getBoolean("wifi"));
            mCheckBoxTv.setChecked(savedInstanceState.getBoolean("tv"));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle saveState) {
        super.onSaveInstanceState(saveState);

        Bundle bnd = new Bundle();
        bnd.putParcelable("accommodation", mAccommodation);
        bnd.putString("date1", mTextView1.getText().toString());
        bnd.putString("date2", mTextView2.getText().toString());
        bnd.putString("adult", mNumberOfAdults.getText().toString());
        bnd.putString("ch0_1", mNumberOfChildren_1.getText().toString());
        bnd.putString("ch2_7", mNumberOfChildren_7.getText().toString());
        bnd.putString("ch8_14", mNumberOfChildren_14.getText().toString());
        bnd.putBoolean("parking", mCheckBoxParking.isChecked());
        bnd.putBoolean("wifi", mCheckBoxWifi.isChecked());
        bnd.putBoolean("tv", mCheckBoxTv.isChecked());

        saveState.putBundle("save_bundle", bnd);
    }

    @Override
    public void onStop() {
        super.onStop();

        Bundle bnd = new Bundle();
        bnd.putParcelable("accommodation", mAccommodation);
        bnd.putString("date1", mTextView1.getText().toString());
        bnd.putString("date2", mTextView2.getText().toString());
        bnd.putString("adult", mNumberOfAdults.getText().toString());
        bnd.putString("ch0_1", mNumberOfChildren_1.getText().toString());
        bnd.putString("ch2_7", mNumberOfChildren_7.getText().toString());
        bnd.putString("ch8_14", mNumberOfChildren_14.getText().toString());
        bnd.putBoolean("parking", mCheckBoxParking.isChecked());
        bnd.putBoolean("wifi", mCheckBoxWifi.isChecked());
        bnd.putBoolean("tv", mCheckBoxTv.isChecked());

        getArguments().putBundle("save_state", bnd);
    }

    private Reservation createReservation() {
        //Reservation(Integer accommodationType, List<Child> children,
        // String dateStart, String dateEnd, Boolean parking, Boolean wifi, Boolean tv

        int accommodationType;
        if(mAccommodation.getType().equals("ROOM")) {
            accommodationType = EnumDictionary.RoomID.ROOM;
        } else {
            accommodationType = EnumDictionary.RoomID.APARTMENT;
        }

        int adults = Integer.parseInt(mNumberOfAdults.getText().toString());

        List<Child> childList = new ArrayList<>(3);
        List<Integer> childNumber = new ArrayList<>(3);

        try {
            childNumber.add(0, Integer.parseInt(mNumberOfChildren_1.getText().toString()));
        } catch (NumberFormatException e) {
            childNumber.add(0, 0);
        }

        try {
            childNumber.add(1, Integer.parseInt(mNumberOfChildren_7.getText().toString()));
        } catch (NumberFormatException e) {
            childNumber.add(1, 0);
        }

        try {
            childNumber.add(2, Integer.parseInt(mNumberOfChildren_14.getText().toString()));
        } catch (NumberFormatException e) {
            childNumber.add(2, 0);
        }


        for(int childIterator = 0; childIterator < 3; childIterator++) {
            String childClass = null;
            switch (childIterator) {
                case 0: childClass = EnumDictionary.AgeClass.C0_1; break;
                case 1: childClass = EnumDictionary.AgeClass.C2_7; break;
                case 2: childClass = EnumDictionary.AgeClass.C8_14; break;
            }

            Child child = new Child(childNumber.get(childIterator), childClass );
            childList.add(childIterator, child);
        }

        String dateStart = mTextView1.getText().toString();
        String dateEnd = mTextView2.getText().toString();

        Boolean parking = mCheckBoxParking.isChecked();
        Boolean wifi = mCheckBoxWifi.isChecked();
        Boolean tv = mCheckBoxTv.isChecked();

        return new Reservation(accommodationType, adults, childList, dateStart, dateEnd, parking, wifi, tv);

    }

}
