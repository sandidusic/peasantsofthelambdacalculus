package opp.fer.hr.hotel_booking.forms;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

public class TokenOrError implements Serializable, Parcelable {

    public final static Parcelable.Creator<TokenOrError> CREATOR = new Creator<TokenOrError>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TokenOrError createFromParcel(Parcel in) {
            return new TokenOrError(in);
        }

        public TokenOrError[] newArray(int size) {
            return (new TokenOrError[size]);
        }

    };
    private final static long serialVersionUID = -2879788798818060241L;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("username")
    @Expose
    private List<String> username = null;
    @SerializedName("password")
    @Expose
    private List<String> password = null;
    @SerializedName("non_field_errors")
    @Expose
    private List<String> nonFieldErrors = null;

    protected TokenOrError(Parcel in) {
        this.token = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.username, (java.lang.String.class.getClassLoader()));
        in.readList(this.password, (java.lang.String.class.getClassLoader()));
        in.readList(this.nonFieldErrors, (java.lang.String.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     */
    public TokenOrError() {
    }

    /**
     * @param username
     * @param token
     * @param nonFieldErrors
     * @param password
     */
    public TokenOrError(String token, List<String> username, List<String> password, List<String> nonFieldErrors) {
        super();
        this.token = token;
        this.username = username;
        this.password = password;
        this.nonFieldErrors = nonFieldErrors;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getUsername() {
        return username;
    }

    public void setUsername(List<String> username) {
        this.username = username;
    }

    public List<String> getPassword() {
        return password;
    }

    public void setPassword(List<String> password) {
        this.password = password;
    }

    public List<String> getNonFieldErrors() {
        return nonFieldErrors;
    }

    public void setNonFieldErrors(List<String> nonFieldErrors) {
        this.nonFieldErrors = nonFieldErrors;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(username).append(token).append(nonFieldErrors).append(password).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TokenOrError) == false) {
            return false;
        }
        TokenOrError rhs = ((TokenOrError) other);
        return new EqualsBuilder().append(username, rhs.username).append(token, rhs.token).append(nonFieldErrors, rhs.nonFieldErrors).append(password, rhs.password).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(token);
        dest.writeList(username);
        dest.writeList(password);
        dest.writeList(nonFieldErrors);
    }

    public int describeContents() {
        return 0;
    }

}
