package opp.fer.hr.hotel_booking.forms;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class LoginInformation implements Serializable, Parcelable
{

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    public final static Parcelable.Creator<LoginInformation> CREATOR = new Creator<LoginInformation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LoginInformation createFromParcel(Parcel in) {
            return new LoginInformation(in);
        }

        public LoginInformation[] newArray(int size) {
            return (new LoginInformation[size]);
        }

    };

    private final static long serialVersionUID = -1064338974656951307L;

    protected LoginInformation(Parcel in) {
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.password = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public LoginInformation() {
    }

    /**
     *
     * @param username
     * @param password
     */
    public LoginInformation(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(username).append(password).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof LoginInformation) == false) {
            return false;
        }
        LoginInformation rhs = ((LoginInformation) other);
        return new EqualsBuilder().append(username, rhs.username).append(password, rhs.password).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(username);
        dest.writeValue(password);
    }

    public int describeContents() {
        return 0;
    }

}
