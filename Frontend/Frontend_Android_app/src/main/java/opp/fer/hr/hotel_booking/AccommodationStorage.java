package opp.fer.hr.hotel_booking;

import android.content.Context;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import opp.fer.hr.hotel_booking.forms.Accommodation;
import opp.fer.hr.hotel_booking.forms.AccommodationType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class AccommodationStorage {
    private static AccommodationStorage accommodationStorage;
    private List<AccommodationType> mAccommodationTypeList;
    private List<Accommodation> mAccommodationList;
    private HashMap<String, LinkedList<CalendarDay>> mReservationDays;

    private AccommodationStorage(Context context) {
        mAccommodationList = new LinkedList<>();
        mAccommodationTypeList = new LinkedList<>();
        mReservationDays = new HashMap<>();
    }

    public static AccommodationStorage get(Context context) {
        if (accommodationStorage == null) {
            accommodationStorage = new AccommodationStorage(context);
        }
        return accommodationStorage;
    }

    public List<Accommodation> getAccommodationList() {
        return mAccommodationList;
    }

    public List<AccommodationType> getAccommodationTypeList() {
        return mAccommodationTypeList;
    }

    public Accommodation getAccommodation(String hashCode) {
        for (Accommodation apartment : mAccommodationList) {
            if (apartment.getId().equals(hashCode)) {
                return apartment;
            }
        }
        return null;
    }

    public void addAccommodation(Accommodation item) {
        if(!mAccommodationList.contains(item))
            mAccommodationList.add(item);
    }

    public AccommodationType getAccommodationType(int hashCode) {
        for (AccommodationType type : mAccommodationTypeList) {
            if(type.getId().equals(hashCode)) {
              return type;
            }
        }
        return null;
    }

    public void addAccommodationType(AccommodationType type) {
        if(!mAccommodationTypeList.contains(type)) {
            mAccommodationTypeList.add(type);
        } else {
            mAccommodationTypeList.remove(type);
            mAccommodationTypeList.add(type);
        }
    }

    public AccommodationType getTypeFromAccommodation(Accommodation accommodation) {
        return getAccommodationType(accommodation.getType());
    }

    public List<Accommodation> getAccommodationsFromType(AccommodationType accommodationType) {
        List<Accommodation> list = new LinkedList<>();
        for(String id : accommodationType.getAccommodations()) {
            list.add(getAccommodation(id));
        }
        return list;
    }

    public static LinkedList<CalendarDay> turnDateRangeIntoDays(String start, String end) {
        Date dateStart = null;
        Date dateEnd = null;
        try {
            dateStart = new SimpleDateFormat("yyyy-MM-dd").parse(start);
            dateEnd = new SimpleDateFormat("yyyy-MM-dd").parse(end);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<Date> list = getDatesBetween(dateStart, dateEnd);
        LinkedList<CalendarDay> exitList = new LinkedList<>();
        for (Date date : list) {
            exitList.add(CalendarDay.from(date.getYear() + 1900, date.getMonth() + 1, date.getDate()));
        }

        return exitList;
    }

    private static List<Date> getDatesBetween(Date startDate, Date endDate) {
        List<Date> datesInRange = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);

        Calendar endCalendar = new GregorianCalendar();

        //Adding one day because we turn [startDate, endDate> into [startDate, endDate]
        Calendar c = Calendar.getInstance();
        c.setTime(endDate);
        c.add(Calendar.DATE, 1);
        endDate = c.getTime();

        endCalendar.setTime(endDate);

        while (calendar.before(endCalendar)) {
            Date result = calendar.getTime();
            datesInRange.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return datesInRange;
    }

    public void addReservation(String id, CalendarDay day) {
        if (mReservationDays.get(id) == null)
            mReservationDays.put(id, new LinkedList<CalendarDay>());

        if (!mReservationDays.get(id).contains(day))
            mReservationDays.get(id).add(day);
    }

    public LinkedList<CalendarDay> getAccommodationReservations(String id) {
        return mReservationDays.get(id);
    }
}
