package opp.fer.hr.hotel_booking;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class ApartmentListActivity extends SingleFragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Fragment AllocateFragment() {
        return new ApartmentListFragment();
    }
}
