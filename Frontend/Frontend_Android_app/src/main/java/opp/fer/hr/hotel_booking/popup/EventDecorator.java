package opp.fer.hr.hotel_booking.popup;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;
import opp.fer.hr.hotel_booking.AccommodationStorage;
import opp.fer.hr.hotel_booking.forms.Accommodation;

import java.util.LinkedList;

public class EventDecorator implements DayViewDecorator {

    private LinkedList<CalendarDay> dates;

    public EventDecorator(Context context, Accommodation accommodation) {
        this.dates = AccommodationStorage.get(context).getAccommodationReservations(accommodation.getId());
    }

    @Override
    public boolean shouldDecorate(@NonNull CalendarDay day) {
        if (dates == null) {
            return false;
        }

        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.addSpan(new DotSpan(10, Color.RED));
        //view.setDaysDisabled(true);
    }


}