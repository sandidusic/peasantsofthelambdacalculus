package opp.fer.hr.hotel_booking;

import android.support.v4.app.Fragment;

public interface FragmentAllocator {
    Fragment AllocateFragment();
}
