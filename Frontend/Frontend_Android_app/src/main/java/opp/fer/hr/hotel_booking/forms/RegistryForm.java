package opp.fer.hr.hotel_booking.forms;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class RegistryForm implements Serializable, Parcelable
{

    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    public final static Parcelable.Creator<RegistryForm> CREATOR = new Creator<RegistryForm>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RegistryForm createFromParcel(Parcel in) {
            return new RegistryForm(in);
        }

        public RegistryForm[] newArray(int size) {
            return (new RegistryForm[size]);
        }

    };

    private final static long serialVersionUID = -6367417409448730486L;

    protected RegistryForm(Parcel in) {
        this.password = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
        this.lastName = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.city = ((String) in.readValue((String.class.getClassLoader())));
        this.country = ((String) in.readValue((String.class.getClassLoader())));
        this.telephone = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public RegistryForm() {
    }

    /**
     *
     * @param lastName
     * @param address
     * @param email
     * @param firstName
     * @param telephone
     * @param password
     * @param country
     * @param city
     */
    public RegistryForm(String password, String email, String firstName, String lastName, String address, String city, String country, String telephone) {
        super();
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.country = country;
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(lastName).append(address).append(email).append(firstName).append(telephone).append(password).append(country).append(city).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RegistryForm) == false) {
            return false;
        }
        RegistryForm rhs = ((RegistryForm) other);
        return new EqualsBuilder().append(lastName, rhs.lastName).append(address, rhs.address).append(email, rhs.email).append(firstName, rhs.firstName).append(telephone, rhs.telephone).append(password, rhs.password).append(country, rhs.country).append(city, rhs.city).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(password);
        dest.writeValue(email);
        dest.writeValue(firstName);
        dest.writeValue(lastName);
        dest.writeValue(address);
        dest.writeValue(city);
        dest.writeValue(country);
        dest.writeValue(telephone);
    }

    public int describeContents() {
        return 0;
    }

}
