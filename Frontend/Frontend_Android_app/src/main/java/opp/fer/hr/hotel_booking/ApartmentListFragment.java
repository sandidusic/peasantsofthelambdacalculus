package opp.fer.hr.hotel_booking;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import okhttp3.ResponseBody;
import opp.fer.hr.hotel_booking.forms.Accommodation;
import opp.fer.hr.hotel_booking.forms.AccommodationType;
import opp.fer.hr.hotel_booking.forms.ErrorMessage;
import opp.fer.hr.hotel_booking.forms.ReservationTime;
import opp.fer.hr.hotel_booking.popup.LoginDialogFragment;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Response;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.List;

public class ApartmentListFragment extends Fragment {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private AccommodationAdapter accommodationAdapter;
    private List<Integer> holdersChanged;

    private TextView optionsMenuRegisterLogin;

    private Handler handler;
    private OnlineAPI client;

    private boolean dataLoadedAccomm;
    private boolean dataLoadedType;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = OnlineClient.get(OnlineClient.ClientSecurity.PUBLIC);
        setHasOptionsMenu(true);
        holdersChanged = new LinkedList<>();
        handler = new Handler();
        dataLoadedAccomm = false;
        dataLoadedType = false;
    }


    @Override
    public void onResume() {
        super.onResume();
        client = OnlineClient.get(OnlineClient.ClientSecurity.PUBLIC);
        setHasOptionsMenu(true);
        UpdateUI();
        holdersChanged = new LinkedList<>();
        handler = new Handler();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_apartment_list, container, false);

        recyclerView = v.findViewById(R.id.apartment_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        refreshLayout = v.findViewById(R.id.swipe_container);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        MakeRESTCalls();
                        UpdateUI();

                        if (refreshLayout.isRefreshing())
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    refreshLayout.setRefreshing(false);
                                }
                            });
                    }
                });
            }
        });

        refreshLayout.setColorSchemeResources(
                android.R.color.holo_orange_light,
                android.R.color.holo_orange_dark);

        UpdateUI();
        return v;
    }

    private void UpdateOptionsMenu() {
        if (optionsMenuRegisterLogin != null)
            if (OnlineClient.getTOKEN() == null) {
                optionsMenuRegisterLogin.setText(getString(R.string.dropdown_register));
            }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        boolean loggedIn = OnlineClient.getTOKEN() != null;

        MenuItem itemLogout = menu.findItem(R.id.menu_dropdown_logout);
        optionsMenuRegisterLogin = (TextView) menu.findItem(R.id.menu_dropdown_register).getActionView();

        itemLogout.setEnabled(loggedIn);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        boolean loggedIn = OnlineClient.getTOKEN() != null;
        MenuItem itemRegister = menu.findItem(R.id.menu_dropdown_register);
        MenuItem itemLogin = menu.findItem(R.id.menu_login_dropdown);
        MenuItem itemLogout = menu.findItem(R.id.menu_dropdown_logout);


        itemRegister.setEnabled(!loggedIn);
        itemLogin.setEnabled(!loggedIn);
        itemLogout.setEnabled(loggedIn);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        UpdateOptionsMenu();

        switch (item.getItemId()) {
            case R.id.menu_dropdown_register: {
                startActivity(RegisterActivity.makeIntent(getContext()));
                break;
            }
            case R.id.menu_dropdown_logout: {
                if (OnlineClient.getTOKEN() == null) {
                    Toast.makeText(getActivity(), "Niste prijavljeni", Toast.LENGTH_SHORT).show();
                } else {
                    OnlineClient.setTOKEN(null);
                    Toast.makeText(getActivity(), "Odjavili ste se", Toast.LENGTH_SHORT).show();
                    getActivity().invalidateOptionsMenu();
                }
                break;
            }
            case R.id.menu_login_dropdown: {
                if (OnlineClient.getTOKEN() != null) {
                    Toast.makeText(getActivity(), "Već ste se prijavili", Toast.LENGTH_SHORT).show();
                }

                DialogFragment newFragment = new LoginDialogFragment();
                newFragment.show(getFragmentManager(), "Login");
                break;
            }
            case R.id.menu_dropdown_about: {
                View rootView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_apartment_list,
                        null, false);
                View v = LayoutInflater.from(getContext()).inflate(R.layout.popup_window, null, false);

                final PopupWindow pw = new PopupWindow(v, 500, 400, true);
                pw.setOutsideTouchable(true);
                TextView textView = v.findViewById(R.id.text_id_popup);
                Button button = v.findViewById(R.id.confirm_button_popup);
                textView.setText(R.string.about_text);
                textView.setTextColor(Color.BLACK);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pw.dismiss();
                    }
                });

                pw.showAtLocation(rootView.findViewById(R.id.apartment_recycler_view), Gravity.CENTER, 0, 0);
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void MakeRESTCalls()
    {

        final AccommodationStorage accommodationStorageReference = AccommodationStorage.get(getActivity());

        Thread retrofitCallThreadAccomm = new Thread(new Runnable() {
            @Override
            public void run() {
                Call<List<Accommodation>> callAccom = client.getAccommodations();

                try {
                    Response<List<Accommodation>> response = callAccom.execute();

                    if (response.isSuccessful()) {
                        List<Accommodation> apartments = response.body();

                        if (accommodationStorageReference.getAccommodationList() != null)
                            accommodationStorageReference.getAccommodationList().clear();
                        for (Accommodation apartment : apartments) {
                            accommodationStorageReference.addAccommodation(apartment);

                            if (accommodationStorageReference.getAccommodationReservations(apartment.getId()) != null)
                                accommodationStorageReference.getAccommodationReservations(apartment.getId()).clear();
                            for (ReservationTime res : apartment.getReservations()) {
                                LinkedList<CalendarDay> list =
                                        AccommodationStorage.
                                                turnDateRangeIntoDays(res.getDateStart(), res.getDateEnd());

                                for (CalendarDay day : list)
                                    accommodationStorageReference.addReservation(apartment.getId(), day);
                            }
                        }

                        dataLoadedAccomm = true;
                    } else {
                        Converter<ResponseBody, ErrorMessage> errorConverter =
                                OnlineClient.getRetrofit().responseBodyConverter(ErrorMessage.class, new Annotation[0]);

                        try {
                            ErrorMessage error = errorConverter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        dataLoadedAccomm = false;
                    }

                    accommodationAdapter = new AccommodationAdapter(accommodationStorageReference.getAccommodationList(),
                            accommodationStorageReference.getAccommodationTypeList());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setAdapter(accommodationAdapter);
                        }
                    });
                } catch (IOException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(), "Greška u spajanju na internet", Toast.LENGTH_SHORT).show();
                        }
                    });
                    accommodationAdapter = new AccommodationAdapter(accommodationStorageReference.getAccommodationList(),
                            accommodationStorageReference.getAccommodationTypeList());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setAdapter(accommodationAdapter);
                        }
                    });
                    dataLoadedAccomm = false;
                }

            }
        });
        retrofitCallThreadAccomm.start();


        Thread retrofitCallThreadType = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<List<AccommodationType>> callType = client.getAccommodationTypes();

                    Response<List<AccommodationType>> response = callType.execute();

                    if (response.isSuccessful()) {
                        for (AccommodationType type : response.body())
                            accommodationStorageReference.addAccommodationType(type);

                        dataLoadedType = true;
                    } else {
                        Converter<ResponseBody, ErrorMessage> errorConverter =
                                OnlineClient.getRetrofit().responseBodyConverter(ErrorMessage.class, new Annotation[0]);

                        try {
                            ErrorMessage error = errorConverter.convert(response.errorBody());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        dataLoadedType = false;
                    }

                } catch (IOException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(), "Greška u spajanju na internet", Toast.LENGTH_SHORT).show();
                        }
                    });
                    dataLoadedType = false;
                }

            }
        });
        retrofitCallThreadType.start();


        try {
            retrofitCallThreadAccomm.join();
            retrofitCallThreadType.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void UpdateUI() {
        if (accommodationAdapter == null) {
            MakeRESTCalls();
        } else {
            for (Integer instance : holdersChanged) {
                accommodationAdapter.notifyItemChanged(instance);
            }
            holdersChanged.clear();
        }

        UpdateOptionsMenu();
    }


    private class AccommodationHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewTitle;
        public ImageView mApartmentImage;
        public TextView mTextViewType;
        public TextView mTextViewCapacityMin;
        public TextView mTextViewCapacityMax;
        public TextView mTextViewLook;
        public Integer mHolderNumeration;

        public Target blurTarget;

        private Accommodation mHolderApartment;
        private AccommodationType mHolderType;

        public AccommodationHolder(@NonNull View itemView) {
            super(itemView);

            mTextViewTitle = itemView.findViewById(R.id.apartment_title_list);
            mApartmentImage = itemView.findViewById(R.id.apartment_image_background);
            mTextViewCapacityMax = itemView.findViewById(R.id.apartment_capacity_list_max);
            mTextViewCapacityMin = itemView.findViewById(R.id.apartment_capacity_list_min);
            mTextViewType = itemView.findViewById(R.id.apartment_type_room_list);
            mTextViewLook = itemView.findViewById(R.id.apartment_look_at_list);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holdersChanged.add(mHolderNumeration);
                    Intent i = ApartmentActivity.makeIntent(getActivity(), mHolderApartment.getId());
                    startActivity(i);
                }
            });

        }

        public void setAccommodation(@NonNull Accommodation mApartment) {
            this.mHolderApartment = mApartment;
        }

        public void setPosition(int holderNumeration) {
            mHolderNumeration = holderNumeration;
        }

        public void setAccommodationType(AccommodationType mHolderType) {
            this.mHolderType = mHolderType;
        }
    }

    private class AccommodationAdapter extends RecyclerView.Adapter<AccommodationHolder> {

        private List<Accommodation> mAdapterApartments;
        private List<AccommodationType> mAdapterTypes;
        private Handler mHandler;
        final AccommodationStorage accommodationStorageReference;

        public AccommodationAdapter(List<Accommodation> apartments, List<AccommodationType> types) {
            mAdapterApartments = apartments;
            mAdapterTypes = types;
            mHandler = new Handler(Looper.getMainLooper());
            accommodationStorageReference = AccommodationStorage.get(getActivity());
        }

        @NonNull
        @Override
        public AccommodationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View v = inflater.inflate(R.layout.list_item_apartment, viewGroup, false);
            return new AccommodationHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final AccommodationHolder accommodationHolder, int i) {

            /*if (accommodationHolder.blurTarget == null) {
                accommodationHolder.blurTarget = new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Blurry.with(getContext()).from(bitmap).into(accommodationHolder.mApartmentImage);
                            }
                        });
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                };
            }*/

            //setWaitAnimation(accommodationHolder.itemView);

            Accommodation apartment = mAdapterApartments.get(i);
            AccommodationType type = mAdapterTypes.get(apartment.getType());
            accommodationHolder.setAccommodation(apartment);
            accommodationHolder.setAccommodationType(type);
            accommodationHolder.setPosition(i);
            accommodationHolder.mTextViewTitle.setText(apartment.getId());
            accommodationHolder.mTextViewCapacityMax.setText(Integer.toString(type.getCapacityMax()));
            accommodationHolder.mTextViewCapacityMin.setText(Integer.toString(type.getCapacityMin()));
            accommodationHolder.mTextViewLook.setText(type.getViewTranslated());
            accommodationHolder.mTextViewType.setText(type.getTypeTranslated());
            Picasso.get().load(mAdapterApartments.get(i).getThumbnail())
                    .into(accommodationHolder.mApartmentImage);
            //Picasso.get().load(mAdapterApartments.get(i).getImage()).into(accommodationHolder.blurTarget);
            setAnimation(accommodationHolder.itemView);
        }

        private void setAnimation(final View viewToAnimate) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Animation animation = AnimationUtils.loadAnimation(getContext(), android.R.anim.slide_in_left);
                    viewToAnimate.startAnimation(animation);
                }
            });
        }

        /*
        private void setWaitAnimation(final View viewToAnimate) {

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Animation fadeout = new AlphaAnimation(1.f, 0.f);

                    while (!(dataLoadedAccomm && dataLoadedType)) {
                        fadeout.setDuration(500);
                        viewToAnimate.startAnimation(fadeout);
                        viewToAnimate.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                viewToAnimate.setVisibility(View.GONE);
                            }
                        }, 500);
                    }
                }
            });
        }
        */

        @Override
        public int getItemCount() {
            return mAdapterApartments.size();
        }
    }
}
