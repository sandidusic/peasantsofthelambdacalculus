package opp.fer.hr.hotel_booking;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.google.gson.Gson;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;
import opp.fer.hr.hotel_booking.forms.ErrorMessage;
import opp.fer.hr.hotel_booking.forms.RegistryForm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment {

    public static final String ARG_REGISTER_ID = "arg_register_id"; //Not needed currently, no arguments

    private ScrollView mScrollView;

    private EditText mEmailEdit;
    private EditText mPasswordEdit;
    private EditText mNameEdit;
    private EditText mSurnameEdit;
    private EditText mAddressEdit;
    private EditText mHouseNumberEdit;
    private EditText mCityEdit;
    private TextView mCountryText;
    private EditText mPhoneNumberEdit;

    private LinearLayout mCountryLayout;

    private CountryPicker mCountryPicker;

    private Button mButtonRegister;

    private OnlineAPI client;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = OnlineClient.get(OnlineClient.ClientSecurity.PUBLIC);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, container, false);

        mScrollView = v.findViewById(R.id.scroll_register);

        mEmailEdit = v.findViewById(R.id.register_email_edit);
        mPasswordEdit = v.findViewById(R.id.register_password_edit);
        mNameEdit = v.findViewById(R.id.register_name_edit);
        mSurnameEdit = v.findViewById(R.id.register_surname_edit);
        mAddressEdit = v.findViewById(R.id.register_address_edit);
        mHouseNumberEdit = v.findViewById(R.id.register_house_number_edit);
        mCityEdit = v.findViewById(R.id.register_city_edit);
        mCountryText = v.findViewById(R.id.register_country_edit);
        mPhoneNumberEdit = v.findViewById(R.id.register_phone_number_edit);
        mButtonRegister = v.findViewById(R.id.register_button_send_request);

        mCountryLayout = v.findViewById(R.id.linear_layout_country_select);

        View.OnClickListener countryListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CountryPicker picker = CountryPicker.newInstance("Select Country");
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                        mCountryText.setText(code);
                        picker.dismiss();
                    }
                });
                picker.show(getFragmentManager(), "COUNTRY_PICKER");

            }
        };
        mCountryLayout.setOnClickListener(countryListener);
        mCountryText.setOnClickListener(countryListener);

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createRegistryForm();

                Call<ErrorMessage> call = client.postRegistration(createRegistryForm());

                call.enqueue(new Callback<ErrorMessage>() {
                    @Override
                    public void onResponse(Call<ErrorMessage> call, Response<ErrorMessage> response) {
                        if(response.isSuccessful()) {
                            showError("Uspješna registracija!");
                            getActivity().onBackPressed();
                        } else if (response.code() >= 400 && response.code() != 500) {

                            Gson gson = new Gson();
                            ErrorMessage message = null;
                            try {
                                message = gson.fromJson(response.errorBody().charStream(), ErrorMessage.class);

                                switch (message.getCode()) {
                                    case 0:
                                        showError("Krivo unešene informacije, provjerite email");
                                        break;
                                    case 6:
                                        showError(message.getMessage());
                                        break;
                                    case 7:
                                        showError(message.getMessage());
                                        break;
                                    case 9:
                                        showError(message.getMessage());
                                        break;
                                }
                            } catch (IllegalStateException ignored) {
                                showError("Greška u spajanju na internet");
                            }
                        } else {
                            showError("Greška u spajanju na internet");
                        }

                    }

                    @Override
                    public void onFailure(Call<ErrorMessage> call, Throwable t) {
                        showError("Greška u spajanju na internet");
                    }
                });
            }
        });

        return v;
    }

    private void showError(String s) {
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        client = OnlineClient.get(OnlineClient.ClientSecurity.PUBLIC);
    }

    private RegistryForm createRegistryForm() {
        String email = mEmailEdit.getText().toString();
        String password = mPasswordEdit.getText().toString();
        String name = mNameEdit.getText().toString();
        String surName = mSurnameEdit.getText().toString();
        String address = mAddressEdit.getText().toString();
        String houseNumber = mHouseNumberEdit.getText().toString();
        String city = mCityEdit.getText().toString();
        String country = mCountryText.getText().toString();
        String phoneNumber = mPhoneNumberEdit.getText().toString();

        return new RegistryForm(password, email, name, surName,
                houseNumber + " " + address, city, country, phoneNumber);
    }
}
