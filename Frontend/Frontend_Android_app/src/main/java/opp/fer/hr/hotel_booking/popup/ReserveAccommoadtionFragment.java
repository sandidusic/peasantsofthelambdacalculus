package opp.fer.hr.hotel_booking.popup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnRangeSelectedListener;
import opp.fer.hr.hotel_booking.AccommodationStorage;
import opp.fer.hr.hotel_booking.ISO8601;
import opp.fer.hr.hotel_booking.R;
import opp.fer.hr.hotel_booking.forms.Accommodation;

import java.util.Calendar;
import java.util.List;

public class ReserveAccommoadtionFragment extends DialogFragment {

    public static final String ACCOMMODATION_RESERVE_ARG = "opp.fer.hr.hotel_booking_reserve_arg";

    private MaterialCalendarView calendarView;

    private Calendar nextYear;
    private Calendar lastMonth;

    private TextView textView1;
    private TextView textView2;


    private Button mConfrimButton;

    private Accommodation accommodation;

    public static ReserveAccommoadtionFragment newInstance(int viewId1, int viewId2, String accommodationId) {

        Bundle args = new Bundle();
        args.putInt(ACCOMMODATION_RESERVE_ARG + "Id1", viewId1);
        args.putInt(ACCOMMODATION_RESERVE_ARG + "Id2", viewId2);
        args.putString(ACCOMMODATION_RESERVE_ARG + "AId", accommodationId);
        ReserveAccommoadtionFragment fragment = new ReserveAccommoadtionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reserve_dates, container);
    }


    @Override
    public void onViewCreated(View v, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        Bundle bnd = getArguments();
        textView1 = getActivity().findViewById(bnd.getInt(ACCOMMODATION_RESERVE_ARG + "Id1"));
        textView2 = getActivity().findViewById(bnd.getInt(ACCOMMODATION_RESERVE_ARG + "Id2"));
        //textView1 = v.findViewById(R.id.date_reserve_text_view1);
        //textView2 = v.findViewById(R.id.date_reserve_text_view2);

        accommodation = AccommodationStorage.
                get(getContext()).getAccommodation(bnd.getString(ACCOMMODATION_RESERVE_ARG + "AId"));

        nextYear = Calendar.getInstance();
        nextYear.set(today.year + 1, today.month, today.monthDay);
        lastMonth = Calendar.getInstance();
        lastMonth.set(today.year, today.month - 1, today.monthDay);


        mConfrimButton = v.findViewById(R.id.confirm_date_button);
        calendarView = v.findViewById(R.id.calendarViewPick);
        calendarView.addDecorator(new EventDecorator(getContext(), accommodation));

        int minYear = 0;
        int maxYear = 0;
        if (today.month > 9) {
            minYear = today.year + 1;
            maxYear = today.year + 1;
        } else if (today.month < 5) {
            minYear = today.year;
            maxYear = today.year;
        }

        textView1.setText("");
        textView2.setText("");

        calendarView.state().edit()
                .setMinimumDate(CalendarDay.from(minYear,
                        4, lastMonth.getTime().getDate()))
                .setMaximumDate(CalendarDay.from(maxYear,
                        8, nextYear.getTime().getDate()))
                .commit();

        calendarView.setOnRangeSelectedListener(new OnRangeSelectedListener() {
            @Override
            public void onRangeSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull List<CalendarDay> list) {
                CalendarDay start = list.get(0);
                CalendarDay end = list.get(list.size() - 1);

                Calendar calStart = ISO8601.calendarDaytoCalendar(start);
                Calendar calEnd = ISO8601.calendarDaytoCalendar(end);

                String startString = ISO8601.fromCalendar(calStart);
                String endString = ISO8601.fromCalendar(calEnd);

                textView1.setText(startString);
                textView2.setText(endString);
            }
        });

        mConfrimButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(textView1.getText().toString().equals("") || textView2.getText().toString().equals(""))) {
                    dismiss();
                } else {
                    Toast.makeText(getContext(),
                            "Odredite vremenski period u kojem želite rezervirat apartman", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }


}
