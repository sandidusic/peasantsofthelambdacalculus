package opp.fer.hr.hotel_booking.forms;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class Child implements Serializable, Parcelable
{

    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("age_class")
    @Expose
    private String ageClass;
    public final static Parcelable.Creator<Child> CREATOR = new Creator<Child>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Child createFromParcel(Parcel in) {
            return new Child(in);
        }

        public Child[] newArray(int size) {
            return (new Child[size]);
        }

    }
            ;
    private final static long serialVersionUID = -4553085922057345443L;

    protected Child(Parcel in) {
        this.number = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.ageClass = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Child() {
    }

    /**
     *
     * @param ageClass
     * @param number
     */
    public Child(Integer number, String ageClass) {
        super();
        this.number = number;
        this.ageClass = ageClass;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getAgeClass() {
        return ageClass;
    }

    public void setAgeClass(String ageClass) {
        this.ageClass = ageClass;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(ageClass).append(number).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Child) == false) {
            return false;
        }
        Child rhs = ((Child) other);
        return new EqualsBuilder().append(ageClass, rhs.ageClass).append(number, rhs.number).isEquals();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(number);
        dest.writeValue(ageClass);
    }

    public int describeContents() {
        return 0;
    }

}