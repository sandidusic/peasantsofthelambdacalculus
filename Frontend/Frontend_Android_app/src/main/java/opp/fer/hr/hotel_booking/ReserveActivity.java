package opp.fer.hr.hotel_booking;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import opp.fer.hr.hotel_booking.forms.Accommodation;

public class ReserveActivity extends SingleFragmentActivity {

    private static final String EXTRA_RESERVE_ID = "practice.fer.hr.hotel_booking.reserveID";

    public static Intent makeIntent(String apartmentID, Context packageContext) {
        Intent intent = new Intent(packageContext, ReserveActivity.class);
        intent.putExtra(EXTRA_RESERVE_ID, apartmentID);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AccommodationStorage.get(getApplicationContext());
    }

    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }

    }

    @Override
    public Fragment AllocateFragment() {
        return createFragment();
    }

    protected ReserveFragment createFragment() {
        String dataString = getIntent().getStringExtra(EXTRA_RESERVE_ID);
        Accommodation accommodation = AccommodationStorage.get(getApplicationContext()).getAccommodation(dataString);
        return ReserveFragment.newInstance(accommodation);
    }

}
